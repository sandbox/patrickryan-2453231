Module: Reztrip
Author: Patrick Ryan <patrick@fractalonline.com>

Description
===========

The Reztrip module integrates a RezTrip booking widget into your Drupal site.


Requirements
============

This module is for Drupal 7.x

Installation
============

Use the package installer or copy the 'reztrip' module directory into your Drupal sites/all/modules directory.  Once you enable the module, click the link to the Reztrip setup page and follow the instructions listed.

Usage
=====

Once installed and configured, a block is made available to place anywhere via the block administration page.


