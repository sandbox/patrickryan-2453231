Reztrip Booking Module
==========================

The Reztrip Booking Module integrates a RezTrip booking widget into your Drupal site.

This module is for Drupal 7.x

Installation
-----

Use the package installer or copy the 'reztrip' module directory into your Drupal sites/all/modules directory.  Once you enable the module, click the link to the Reztrip setup page and follow the instructions listed.

Usage
-----

Once installed and configured, a block is made available to place anywhere via the block administration page.



