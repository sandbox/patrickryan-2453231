(function($){
  Drupal.behaviors.reztrip = {
    attach: function(context, settings) {

      var arrivalInput = $("#edit-arrival-date");
      var departureInput = $("#edit-departure-date");

      arrivalInput.datepicker({
        dateFormat: 'yy-m-d',
        defaultDate: 0,
        minDate: 0,
        showButtonPanel: false,
        onClose: function() {
          if(arrivalInput.val() == '') {
            arrivalInput.val('yyyy-mm-dd');
          } else {
            var minCheckoutDate = arrivalInput.datepicker('getDate', '+1d');
            minCheckoutDate.setDate(minCheckoutDate.getDate() + 1);
            departureInput.datepicker("option", "minDate", minCheckoutDate);

            if((departureInput.val() == 'yyyy-mm-dd') || (departureInput.val() == '')) {
              Drupal.theme('setDates', arrivalInput, departureInput);
              departureInput.datepicker("show");
            }
          }
        }
      });

      departureInput.datepicker({
        dateFormat: 'yy-m-d',
        defaultDate: 0,
        minDate: 0,
        showButtonPanel: false,
        onClose: function() {
          var maxCheckinDate = departureInput.datepicker('getDate', '-1d');
          maxCheckinDate.setDate(maxCheckinDate.getDate() - 1);
          arrivalInput.datepicker("option", "maxDate", maxCheckinDate);

          if((departureInput.val() == 'yyyy-mm-dd') || (departureInput.val() == '')) {
            departureInput.val('yyyy-mm-dd');
          } else {
            if((arrivalInput.val() == 'yyyy-mm-dd') || (arrivalInput.val() == '')) {
              arrivalInput.datepicker("show");
            }
          }
        }
      });
    }
  }
})(jQuery);

Drupal.theme.prototype.setDates = function(arrivalInput, departureInput) {
  var inDate = arrivalInput.datepicker('getDate');
  var outDate = departureInput.datepicker('getDate');

  if((inDate == 'yyyy-mm-dd') || (inDate == '')) {
    inDate = false;
  } else {
    inDate = new Date(inDate);
  }

  if((outDate == 'yyyy-mm-dd') || (outDate == '')) {
    outDate = false;
  } else {
    outDate = new Date(outDate);
  }

  if(inDate) {
    outDate = new Date(inDate);
    outDate.setDate(outDate.getDate() + 1);
    departureInput.datepicker("setDate", outDate);
  } else {
    arrivalInput.datepicker("show");
  }
};